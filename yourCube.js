'use strict';

var numPosts = "";
var startEntries = 3;

function Blog(body,date,image) {
    this.body = body;
    this.date = date;
    this.image = image;
}

Blog.prototype.signature = "/losos33";

Date.prototype.shortFormat = function() {
    return this.getFullYear() + "/" +
            (this.getMonth() + 1) + "/" +
            this.getDate();
};

Blog.prototype.toHTML = function (highlight, id) {
    let blogHTML = "";
    blogHTML += highlight ? "<p id='" + id + "' style='background-color:gray'>" :
        "<p id='" + id + "'>";
    if (this.image) {
        blogHTML += "<strong>" + this.date.shortFormat() + "</strong><br/><table>" +
            "<td><img src='" + this.image + "' alt='' style='width: 90px" +
            "'/></td><br>" +
            "<td style='vertical-align:top'>" + this.body + "</td></table>" +
            "<em>" + this.signature + "</em></p>";
    } else {
        blogHTML += "<strong>" + this.date.shortFormat() + "</strong><br/>" + this.body + "<br/><em>" +
            this.signature + "</em></p>";
    }
    return blogHTML;
};

Blog.prototype.containsText = function (text) {
    return (this.body.toLowerCase().indexOf(text.toLowerCase()) !== -1);
};

Blog.blogSorter = function (blog1, blog2) {
    return blog2.date - blog1.date
};

var blog = [new Blog("Buy a new cube", new Date("2019/08/14")),
            new Blog("Fell to depression", new Date("2019/08/19")),
            new Blog("Found a 7x7x7 cube for sale online", new Date("2019/08/21"), "cube.jpg"),
            new Blog("Got the cube", new Date("2019/08/11")),
            new Blog("Solved the cube", new Date("2019/08/13")),
            new Blog("Solved the new cube", new Date("2019/08/18")),
            new Blog("Lost the cube", new Date("2019/08/22"))];


let input = document.getElementById("searchText");
input.onkeydown = function (event) { if(event.keyCode === 13){searchBlog()}};

function searchBlog() {
    showAll();
    let isStringFound = false;
    let searchString = document.getElementById('searchText').value;
    for (let i = 0; i < blog.length; i++) {
        if (blog[i].containsText(searchString)) {
            document.getElementById(i).style.backgroundColor = "red";
            if (!isStringFound) {
                isStringFound = true;
            }
        }
    }
    if (isStringFound == false) alert(searchString + " not found.");
}

function fillBlog() {
    blog.sort(Blog.blogSorter);
    let blogListHTML = "";
    for (let i = 0; i < numPosts; i++) {
        blogListHTML += blog[i].toHTML(i%2 == 0, i);
    }
    document.getElementById("blog").innerHTML = blogListHTML;
}
function showBlog() {
    numPosts = startEntries;
    fillBlog();
}
function showRandom() {
    let i = Math.floor(Math.random()*blog.length);
    alert(blog[i]);
}
function showAll() {
    numPosts = blog.length;
    fillBlog();
}
